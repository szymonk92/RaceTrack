package tree;

import java.util.List;

public class NodeBuilder<T> {
    private T data;
    private Node<T> parent;
    private List<Node<T>> children;

    public NodeBuilder setData(T data) {
        this.data = data;
        return this;
    }

    public NodeBuilder setParent(Node<T> parent) {
        this.parent = parent;
        return this;
    }

    public NodeBuilder setChildren(List<Node<T>> children) {
        this.children = children;
        return this;
    }

    public Node createNode() {
        return new Node(data, parent, children);
    }
}