package game;

/**
 * Created by szymo on 12.10.2015.
 */
public class Board {

    private int width; //17
    private int weight; //30

    private int board[][];

    public Board(int width, int weight) {
        this.width = width;
        this.weight = weight;

        board = new int[width][weight];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < weight; j++) {
                board[i][j] = 1;
            }
        }
    }

    public int[][] getBoard() {
        return board;
    }

    //[columns][rows]
    public void prepareBoardOne() {

        ///UP LEFT corner
        for (int i = 0; i < 4; i++) {
            board[i][0] = 0;
        }

        for (int i = 0; i < 3; i++) {
            board[i][1] = 0;
        }

        board[0][2] = 0;

        ///bottom left corner
        for (int i = width - 16; i < width; i++) {
            board[i][0] = 0;
        }
        for (int i = width - 10; i < width; i++) {
            board[i][1] = 0;
        }

        for (int i = width - 3; i < width; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = 0;
            }
        }

        ////////bottom right corner //
        for (int i = 7; i < width; i++) {
            for (int j = weight - 8; j < weight; j++) {
                board[i][j] = 0;
            }
        }
        board[7][weight - 8] = 1;


        ///////START LINE////////
        for (int j = 3; j < 9; j++) {
            board[width - 1][j] = 3;
        }

        ///////FINISH LINE////////
        for (int i = 0; i < 7; i++) {
            board[i][weight - 1] = 2;
        }
    }

    public void printBoard() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < weight; j++) {
                System.out.print(this.board[i][j]);
            }
            System.out.println("");
        }
    }
}
