import game.Board;

/**
 * Created by szymo on 12.10.2015.
 */
public class Application {

    public static void main(String[] args) {

        //0 you cannot go there
        //1 is road
        //2 is final line
        //3 is start line
        Board board = new Board(30, 17);
        board.prepareBoardOne();
        board.printBoard();


    }
}
